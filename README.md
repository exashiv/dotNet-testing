# .Net Sample Project Running on Docker

This is a sample project of asp Dockerized.

First you need to build it:

```shell
docker build -t sample-project path/to/project
```

And then you run it:

```shell
docker run -p 8000:80 -it --rm sample-project
```

